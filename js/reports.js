/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

var stores = Array();

$(document).ready(function () {

    /* disabled filters for now suggested by Raja */
    $(".storeFilter select").attr("disabled", "disabled");

    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");
    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format('MM/DD/YYYY');
        var endDate = moment().endOf('month').format('MM/DD/YYYY');
        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    } else {
        var startDate = sdate;
        var endDate = edate;
        $(".startDate").val(sdate);
        $(".endDate").val(edate);
    }

    // sdate = localStorage.getItem("RS_date");
    // edate = localStorage.getItem("RE_date");
    ///If local storage have startdate or end date then it will show old date
    /*
    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format("MM/DD/YYYY");
        var endDate = moment().endOf('month').format("MM/DD/YYYY");
        // localStorage.setItem("RS_date",startDate);
        // localStorage.setItem("RE_date",endDate);
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
    }
    else {
        var startDate = sdate;
        var endDate = edate;
        $("#startDate").val(sdate);
        $("#endDate").val(edate);
    } */

    loadStoreData(startDate, endDate);
});

$(".add-store a").click(function () {
    localStorage.setItem("backToPage", "reports");
});

////////// to get particular store

$('#searchBtn').click(function () {
    var storeName = $("#searchStore").val();
    if (storeName == '') {
        swal({
            title: "",
            text: "Please Enter store name !",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                // $("#searchText").focus();
            });
    } else {
        store = $.grep(stores, function (i, e) {
            return i.name.toLowerCase() == storeName.toLowerCase();
        });

        if (store.length > 0) {
            loadStore(store);
        } else {
            swal({
                title: "",
                text: "No Store Found !",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {
                    // $("#searchText").focus();
                });
        }
    }
})

function loadStoreData(startDate, endDate) {

    //////////////  To get offer Count and Revenue
    $.ajax({
        url: base_url + "superadmins/offerCountandRevenue",
        data: {
            "startDate": moment(startDate).format("YYYY-MM-DD"),
            "endDate": moment(endDate).format("YYYY-MM-DD"),
            "access_token": access_token
        },

        success: function (data) {
            console.log(data);
            $('#liveOffers').html(data.liveCount);
            $('#enddedOffers').html(data.endedCount);
            $('#allTrans').html("$" + data.totRevenue.toFixed(2));
            $('#cnlTrans').html("$" + data.cxlTxAmount.toFixed(2));
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

    ///////////////// To get store List


    $.ajax({
        url: base_url + "superadmins/storeList",
        data: {
            "startDate": moment(startDate).format("YYYY-MM-DD"),
            "endDate": moment(endDate).format("YYYY-MM-DD"),
            "access_token": access_token
        },
        success: function (data) {
            stores = data;
            loadStore(stores);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

}

function loadStore(data) {
    $('#list').html('');
    $.each(data, function (key, value) {
        //var url = base_url + "bevvi-superadmin/store-transaction?id=" + value.id;
        var dataList = '<tr>' +
            '<td id="' + value.id + '"><a href="' + 'store-transaction?id=' + value.id + '">' + value.name + '</a></td>' +
            '<td>' + value.address + '</td>' +
            '</tr>';
        $('#list').append(dataList);
    });
}

$(".datepicker-head").on("change", function () {
    sDate = $("#startDate").val();
    eDate = $("#endDate").val();
    
    localStorage.setItem("start_date", sDate);
    localStorage.setItem("end_date", eDate);

    // if (sDate > eDate) {
    if (moment(sDate, 'MM/DD/YYYY') > moment(eDate, 'MM/DD/YYYY')) {
        $("#startDate").val(moment().startOf('month').format('MM/DD/YYYY'));
        $("#endDate").val(moment().endOf('month').format('MM/DD/YYYY'));
        //alert("start date should be small then end date");
        swal({
            title: "",
            text: dateErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                // $("#searchText").focus();
                sDate = $("#startDate").val();
                eDate = $("#endDate").val();
                loadStoreData(sDate, eDate);
            });
    } else {
        sDate = $("#startDate").val();
        eDate = $("#endDate").val();
        loadStoreData(sDate, eDate);
    }
});