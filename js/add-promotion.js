/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}
//Accept number only in input
jQuery('.number').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

$(document).ready(function () {

    $(".datepicker").datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: 0
    });
    $(".create-error").hide();
});

$(".btn-create").click(function () {
    var validation = FormValidate();

    var promotionName = $("#promotionName").val();
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var minPrice = $("#minPrice").val();
    var disBy = $("#discountBy").val();
    var disPrice = $("#disPrice").val();
    var code = $("#code").val();
    var description = $("#description").val();

    if (validation == true) {
        //Add last minute of selected date when end date is added.
        startDate = moment(startDate + " 00:00:00", "MM/DD/YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss');
        endDate = moment(endDate + " 23:59:59", "MM/DD/YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss');

        //Valid form
        $(".create-error").hide();
        if (disBy == 'A') {
            data = {
                "name": promotionName,
                "description": description,
                "startsAt": startDate,
                "endsAt": endDate,
                "discAmount": disPrice,
                "minimumAmt": minPrice,
                "code": code,
                "active": true
                // "establishmentId" : '5a24a28310785a5c1aa51c79'
            };
        } else {
            data = {
                "name": promotionName,
                "description": description,
                "startsAt": startDate,
                "endsAt": endDate,
                "discPercentage": disPrice,
                "minimumAmt": minPrice,
                "code": code,
                "active": true
                //"establishmentId" : '5a24a28310785a5c1aa51c79'
            };
        }
        console.log(data);
        swal({
            title: "",
            text: "Create a New Promotion?",
            //type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Create",
            closeOnConfirm: true
        },
            function () {
                //Submit Data
                $.ajax({
                    url: base_url + 'promotions',
                    type: "POST",
                    dataType: "json",
                    data: data,
                    success: function (response) {
                        console.log(response);
                        window.location = 'promotions';
                        // $.ajax({
                        //     url: base_url + 'superadmins/publishPromotion?access_token=' + access_token + '&promoId=' + response.id,
                        //     type: "POST",
                        //     dataType: "json",
                        //     //data: data,
                        //     complete: function (responses) {
                        //         if (responses.status == 200) {
                        //             window.location = 'promotions';
                        //         } else {
                        //             swal({
                        //                 title: "",
                        //                 text: errorMessage,
                        //                 type: "error",
                        //                 showCancelButton: false,
                        //                 confirmButtonClass: "btn-danger",
                        //                 confirmButtonText: "Ok",
                        //                 closeOnConfirm: false
                        //             },
                        //                 function () {
                        //                     localStorage.removeItem("superUserId");
                        //                     window.location = 'login';
                        //                 });
                        //         }
                        //     }
                        // });

                    },
                    error: function (response) {
                        console.log(response);
                        if (response.status == 401) {
                            swal({
                                title: "",
                                text: errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: false
                            },
                                function () {
                                    localStorage.removeItem("superUserId");
                                    window.location = 'login';
                                });
                        }
                    }
                });
            });
    } else {
        // invalid form
        $(".create-error").html("* All Field Required !");
        $(".create-error").show();
    }
});

function FormValidate() {

    var valid = true;

    var promotionName = $("#promotionName").val();
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var minPrice = $("#minPrice").val();
    var disBy = $("#discountBy").val();
    var disPrice = $("#disPrice").val();
    var code = $("#code").val();
    var description = $("#description").val();

    startDate = moment(startDate + " 00:00:00", "MM/DD/YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss');
    endDate = moment(endDate + " 23:59:59", "MM/DD/YYYY HH:mm:ss").utc().format('YYYY-MM-DD HH:mm:ss');

    if (promotionName === '') {
        valid = false;
    }
    if (startDate === '') {
        valid = false;
    }
    if (endDate === '') {
        valid = false;
    }
    if (startDate != '' && endDate != '') {
        if (startDate > endDate) {
            swal("", dateErrorMessage, "error");
            valid = false;
        } else {
            valid = true;
        }
    }
    if (minPrice === '') {
        valid = false;
    }
    if (disBy === '') {
        valid = false;
    }
    if (disPrice === '') {
        valid = false;
    }
    if (code === '') {
        valid = false;
    }
    if (description === '') {
        valid = false;
    }

    //Validate form
    if (valid === false) {
        return false;
    } else {
        return true;
    }
}