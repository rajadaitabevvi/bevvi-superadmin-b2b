/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

var orders = Array();

$(document).ready(function () {
    sDate = moment(localStorage.getItem("MOS_date")).format('YYYY-MM-DD');
    eDate = moment(localStorage.getItem("MOE_date")).format('YYYY-MM-DD');
    $.ajax({
        // url: base_url + 'orders?filter={"where": {"establishmentId": "' + params + '","deliveryTime":{"between":["' + sDate + '","' + eDate + '"]}}, "include":["account","establishment",{"orderdetails":["product"]}]}',
        url: base_url + 'orders?filter={"where": {"establishmentId": "' + params + '"}, "include":["account","establishment",{"orderdetails":["product"]}]}',
        success: function (data) {
            orders = sortResults(data, 'updatedAt', false);
            $(".mo-head h2").html(data[0].establishment.name);

            /*
            0-orders
            1-ready to Deliver
            2- fullfilled
            3&4 - past orders
             */
            liveOrders = $.grep(orders, function (i, e) {
                return i.status == 0;
            });
            liveOrders = sortResults(liveOrders, 'deliveryTime', true);
            loadData(liveOrders, 0);

            rtp_orders = $.grep(orders, function (i, e) {
                return i.status == 1;
            });
            rtp_orders = sortResults(rtp_orders, 'updatedAt', false);
            loadData(rtp_orders, 1);

            fulfilled_orders = $.grep(orders, function (i, e) {
                return i.status == 2;
            });
            fulfilled_orders = sortResults(fulfilled_orders, 'updatedAt', false);
            loadData(fulfilled_orders, 2);

            past_orders = $.grep(orders, function (i, e) {
                return i.status == 3 || i.status == 4;
            });
            past_orders = sortResults(past_orders, 'updatedAt', false);
            loadData(past_orders, 3);
        },
        error: function (response) {
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
});

function loadData(orderDetail, type) {
    switch (type) {
        case 0: //Orders
            if (orderDetail.length > 0) {
                $(".orders tbody").html('');
                $.each(orderDetail, function (key, val) {
                    if (jQuery.inArray(val.account.firstName) == -1) {
                        name = val.account.firstName;
                    } else {
                        name = '';
                    }
                    liveOrders = '<tr class="oList" id="' + val.orderNumber + '">' +
                        '<td><span>' + val.orderNumber + '</span>' +
                        '<br>' + val.qty + ' items for ' + name + '</td>' +
                        '<td><span>' + moment().to(moment(val.deliveryTime), true) + '</span></td>' +
                        '</tr>';
                    $(".orders tbody").append(liveOrders);
                });
            } else {
                $(".orders tbody").append('<tr><td colspan="2"> No Orders !</td></tr>');
            }
            break;

        case 1: //Ready to Deliver
            $(".rtp_orders tbody").html('');
            if (orderDetail.length > 0) {
                $.each(orderDetail, function (key, val) {
                    if (val.account.firstName) {
                        name = val.account.firstName;
                    } else {
                        name = '';
                    }
                    rtp_orders = '<tr class="oList" id="' + val.orderNumber + '">' +
                        '<td><span>' + val.orderNumber + '</span>' +
                        '<br>' + val.qty + ' items for ' + name + '</td>' +
                        '<td>' + moment(val.deliveryTime).format("hh:mm a") + '</td>' +
                        '</tr>';
                    $(".rtp_orders tbody").append(rtp_orders);
                });
            } else {
                $(".rtp_orders tbody").append('<tr><td colspan="2"> No Orders !</td></tr>');
            }
            break;

        case 2: //fulfilled
            $(".fulfilled_orders tbody").html('');
            if (orderDetail.length > 0) {
                $.each(orderDetail, function (key, val) {
                    if (val.account.firstName) {
                        name = val.account.firstName;
                    } else {
                        name = '';
                    }
                    fulfilled_orders = '<tr class="oList" id="' + val.orderNumber + '">' +
                        '<td><span>' + val.orderNumber + '</span>' +
                        '<br>' + val.qty + ' items for ' + name + '</td>' +
                        '<td>' + 'Fulfilled' + '</td>' +
                        '</tr>';
                    $(".fulfilled_orders tbody").append(fulfilled_orders);
                });
            } else {
                $(".fulfilled_orders tbody").append('<tr><td colspan="2"> No Orders !</td></tr>');
            }
            break;

        case 3: //past Orders
            $(".past_orders tbody").html('');
            if (orderDetail.length > 0) {
                $.each(orderDetail, function (key, val) {
                    if (val.status == 2) {
                        status = 'Fulfilled';
                    } else if (val.status == 3) {
                        status = 'Rejected';
                    } else if (val.status == 4) {
                        status = 'Cancelled';
                    }
                    if (val.account.firstName) {
                        name = val.account.firstName;
                    } else {
                        name = '';
                    }
                    past_orders = '<tr class="oList" id="' + val.orderNumber + '">' +
                        '<td><span>' + val.orderNumber + '</span>' +
                        '<br>' + val.qty + ' items for ' + name + '</td>' +
                        '<td>' + status + '</td>' +
                        '<td>' + moment(val.deliveryTime).format("MMM.DD") + '</td>' +
                        '</tr>';
                    $(".past_orders tbody").append(past_orders);
                });
            } else {
                $(".past_orders tbody").append('<tr><td colspan="3"> No Orders !</td></tr>');
            }
            break;
    }
}

$(document.body).on("click", ".oList", function () {
    id = $(this).attr("id");
    window.location = 'live-orders?id=' + id;
});


//Search result
$(".searchBtn").click(function () {

    var search = $("#searchText").val().trim();
    var result = '';
    if (search == '') {
        swal({
            title: "",
            text: "Please Enter confirmation Code !",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                $("#searchText").focus();
            });
        ///swal("error",,"error");
    } else {
        //Call API with where condition of orderNumber
        $.ajax({
            url: base_url + 'orders?filter={"where" : {"orderNumber" : "' + search + '"}}',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    window.location = 'view-receipt?id=' + search
                } else {
                    swal({
                        title: "",
                        text: "Invalid Confirmation Code !",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            // $("#searchText").focus();
                        });
                }
            },
            error: function (response) {
                console.log(response);
                if (response.status == 401) {
                    swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                        function () {
                            localStorage.removeItem("superUserId");
                            window.location = 'login';
                        });
                }
            }
        });
    }
});