/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

/*store hours*/
var days = {
    2: "Monday",
    3: "Tuesday",
    4: "Wednesday",
    5: "Thursday",
    6: "Friday",
    7: "Saturday",
    1: "Sunday"
};
bizAccountId = '';
establishmentId = params;

//Accept number only in input
jQuery('.number').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

///////// window load function
$(document).ready(function () {

    $.ajax({
        url: base_url + "establishments/" + params + '?filter={"where":{"bevviActive": true}, "include":["bizaccount"]}',

        success: function (data) {
            console.log(data);

            $('#storeName').html(data.name); //Title Name
            ////// Display general information

            $('#gi_name').val(data.name);
            $('#gi_address').val(data.address);
            $('#gi_phone').val(data.phoneNum); //not getting phone number
            $('#gi_email').val(data.email);
            $('#gi_website').val(data.url);

            /////// for admin information
            $('#ad_name').val(data.bizaccount.firstName); //data.name
            $('#ad_accNo').val(data.bizaccount.accountNum); //data.accountId
            $('#ad_email').val(data.bizaccount.email);
            $('#ad_phone').val(data.bizaccount.phoneNum);
            bizAccountId = data.bizaccount.id;

            $.getJSON(base_url + 'commissions?filter={"where":{"bizAccountId":"' + bizAccountId + '"}}', function (result) {
                if (result.length) {
                    result = result[0];
                    chargePerUnit = $("#chargePerUnit").val(result.chargePerUnit);
                    tier1_revenue = $("#tier1_revenue").val(result.tiers.tier1.revenue);
                    tier1_fee = $("#tier1_fee").val(result.tiers.tier1.fee);
                    tier2_revenue = $("#tier2_revenue").val(result.tiers.tier2.revenue);
                    tier2_fee = $("#tier2_fee").val(result.tiers.tier2.fee);
                } else {
                    chargePerUnit = $("#chargePerUnit").val('0');
                    tier1_revenue = $("#tier1_revenue").val('0');
                    tier1_fee = $("#tier1_fee").val('0');
                    tier2_revenue = $("#tier2_revenue").val('0');
                    tier2_fee = $("#tier2_fee").val('0');
                }
            });

            ////////// for store hours
            $.each(data.operatesAt.timeframes, function (i, time) {
                var start_time = time.open[0].start;
                var end_time = time.open[0].end;

                $.each(time.days, function (k, day) {
                    switch (parseInt(day)) {
                        case 2:
                            $("#mon .start").val(moment(start_time, 'h:mm a').format('h:mm A')); // 10:00 AM
                            $("#mon .end").val(moment(end_time, 'h:mm a').format('h:mm A')); // 8:00 PM
                            break;
                        case 3:
                            $("#tue .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#tue .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                        case 4:
                            $("#wed .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#wed .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                        case 5:
                            $("#thu .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#thu .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                        case 6:
                            $("#fri .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#fri .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                        case 7:
                            $("#sat .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#sat .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                        case 1:
                            $("#sun .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                            $("#sun .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                            break;
                    }
                });
            });
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

});


function timeConvert(time) {
    if (time == '') {
        time = "00:00 AM"
    }
    return moment(time, 'h:m A').format('HHmm');
}

$("#btnSave").click(function () {

    var mon = [timeConvert($('#mon .start').val()), timeConvert($('#mon .end').val())];
    var tue = [timeConvert($('#tue .start').val()), timeConvert($('#tue .end').val())];
    var wed = [timeConvert($('#wed .start').val()), timeConvert($('#wed .end').val())];
    var thu = [timeConvert($('#thu .start').val()), timeConvert($('#thu .end').val())];
    var fri = [timeConvert($('#fri .start').val()), timeConvert($('#fri .end').val())];
    var sat = [timeConvert($('#sat .start').val()), timeConvert($('#sat .end').val())];
    var sun = [timeConvert($('#sun .start').val()), timeConvert($('#sun .end').val())];

    //In mobile week sunday to saturday
    var schedule = {
        "timeframes": [{
                "days": [
                    2
                ],
                "open": [{
                    "start": mon[0],
                    "end": mon[1]
                }]
            },
            {
                "days": [
                    3
                ],
                "open": [{
                    "start": tue[0],
                    "end": tue[1]
                }]
            },
            {
                "days": [
                    4
                ],
                "open": [{
                    "start": wed[0],
                    "end": wed[1]
                }]
            },
            {
                "days": [
                    5
                ],
                "open": [{
                    "start": thu[0],
                    "end": thu[1]
                }]
            },
            {
                "days": [
                    6
                ],
                "open": [{
                    "start": fri[0],
                    "end": fri[1]
                }]
            },
            { //Saturday
                "days": [
                    7
                ],
                "open": [{
                    "start": sat[0],
                    "end": sat[1]
                }]
            },
            { //Sunday
                "days": [
                    1
                ],
                "open": [{
                    "start": sun[0],
                    "end": sun[1]
                }]
            }
        ]
    };

    // //Save BizAccount details
    $.ajax({
        url: base_url + 'bizaccounts/' + bizAccountId + '?access_token=' + access_token,
        data: {
            firstname: $('#ad_name').val(),
            email: $('#ad_email').val(),
            phoneNum: $('#ad_phone').val(),
            accountNum: $('#ad_accNo').val(),
            establishmentId: [params]
        },
        type: "PATCH",
        dataType: "json",
        success: function (data) {
            //alert("Account Updated Successfully.");
        }
    });

    // // //Save Establishment details
    $.ajax({
        url: base_url + 'establishments/' + params,
        data: {
            name: $('#gi_name').val(),
            url: $('#gi_website').val(),
            address: $('#gi_address').val(),
            phoneNum: $('#gi_phone').val(),
            email: $('#gi_email').val(),
            operatesAt: schedule,
            bizAccountId: bizAccountId
        },
        type: "PATCH",
        dataType: "json",
        success: function (response) {
            console.log(response);
            $.ajax({
                url: base_url + 'commissions/upsertWithWhere?where={ "bizAccountId" : "' + bizAccountId + '"}',
                data: {
                    bizAccountId: bizAccountId,
                    chargePerUnit: $("#chargePerUnit").val(),
                    tiers: {
                        tier1: {
                            revenue: $("#tier1_revenue").val(),
                            fee: $("#tier1_fee").val()
                        },
                        tier2: {
                            revenue: $("#tier2_revenue").val(),
                            fee: $("#tier2_fee").val()
                        }
                    }
                },
                type: "POST",
                dataType: "json",
                success: function (data) {
                    swal({
                            title: "",
                            text: "Details Updated Successfully.",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                        function () {
                            //window.location.reload();
                            window.location = 'setting';
                        });
                }
            });

            //alert("Details Updated Successfully.");
        }
    });
});