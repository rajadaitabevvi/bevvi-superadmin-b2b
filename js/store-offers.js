$(document).ready(function () {

    var startDate = moment().startOf('month').format('YYYY-MM-DD');
    var endDate = moment().format('YYYY-MM-DD');
    var sdate = moment().startOf('month').format('L');
    var edate = moment().format('L');

    startDate = "2017-02-02";
    endDate = "2018-12-31";
    sdate = "02/02/2017";
    edate = "12/31/2018";

    $.ajax({
        url: base_url + "establishments/offers?establishmentId=5a24a28510785a5c1aa532d6&startDate=" + startDate + "&endDate=" + endDate + "&access_token=" + access_token,

        success: function (data) {
            console.log(data);
            $('#startDate').datepicker().val(sdate);
            $('#endDate').datepicker().val(edate);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

});