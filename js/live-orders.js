/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

///Cancel Button Click
function cancelOrder() {
    $.ajax({
        url: base_url + 'orders/' + $("#cancelBtn").attr('data-id'),
        data: {
            status: 4
        },
        type: "PATCH",
        success: function (data) {
            console.log(data);
            swal({
                    title: "",
                    text: "Order Cancelled Successfully.",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                function () {
                    window.location = 'view-orders?id=' + $(".storeName").attr('data-id');
                });
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
}


$(document).ready(function () {

    $.ajax({
        // url: base_url + 'orders?filter={"include":["account",{"orderdetails":["product"]}],"where":{"orderNumber":"' + orderNo + '"}}',
        url: base_url + 'orders?filter={"where": {"orderNumber": "' + params + '"}, "include":["establishment",{"orderdetails":["product"]}]}',
        success: function (data) {
            data = data[0];
            console.log(data);
            $('.storeName').html(data.establishment.name);
            $('.storeName').attr('data-id', data.establishment.id);

            $('.orderNo').html(params);
            $('#viewReceipt').attr('href', 'view-receipt?id=' + params);


            breadcrumb = '<li><a href="manage-orders">Manage Orders </a></li>' +
                '<li><a href="view-orders?id=' + data.establishmentId + '">View Orders </a></li>' +
                '<li class="active" id="orderStatus">Live Order Detail</li>';
            $(".breadcrumb").html(breadcrumb);

            cancelBtn = '';
            if (data.status == 0) {
                status = 'Processing';
                $("#orderStatus").html("Live Order Detail");
                cancelBtn = '<a href="#" onclick="cancelOrder()" id="cancelBtn" data-id="' + data.id + '">Cancel</a>';
            } else if (data.status == 1) {
                status = 'Ready to Deliver';
                $("#orderStatus").html("Live Order Detail");
                cancelBtn = '<a href="#" onclick="cancelOrder()" id="cancelBtn" data-id="' + data.id + '">Cancel</a>';
            } else if (data.status == 2) {
                status = 'Fulfilled';
                $("#orderStatus").html("Past Order Detail");
                cancelBtn = '';
            } else if (data.status == 3) {
                status = 'Rejected';
                $("#orderStatus").html("Past Order Detail");
                cancelBtn = '';
            } else if (data.status == 4) {
                status = 'Cancelled';
                $("#orderStatus").html("Past Order Detail");
                cancelBtn = '';
            }

            $.each(data.orderdetails, function (key, value) {

                var listingData = '<div class="row mar-top40">' +
                    '<div class="col-sm-12">' +
                    '<div class="card card-1">' +
                    '<div>' +
                    '<img src="images/bottle.png" alt="">' +
                    '</div>' +
                    '<div class="product-info-1">' +
                    '<div>' +
                    '<p><span class="offer-main">Product</span>' + value.product.name + '</p>' +
                    '</div>' +
                    '<div>' +
                    '<p class="status"><span class="offer-main">Status</span>' + status + '</p>' +
                    '</div>' +
                    '<div>' +
                    '<p><span class="offer-main">UPC Code</span>' + value.product.upc + '</p>' +
                    '</div>' +
                    '<div>' +
                    '<p> <span class="offer-main">Order number</span>' + data.orderNumber + '</p> ' +
                    '</div>' +
                    '<div>' +
                    '<p> <span class="offer-main">Item Category</span>' + value.product.category + '</p> ' +
                    '</div>' +
                    '<div></div> ' +
                    '<div>' +
                    '<p> <span class="offer-main">Original Price</span> $' + value.actualPrice.toFixed(2) + '</p>' +
                    '</div> ' +
                    '<div></div> ' +
                    '<div> ' +
                    '<p> <span class="offer-main">Discount Price</span> $' + value.price.toFixed(2) + '</p>' +
                    '</div>' +
                    '<div></div> ' +
                    '<div>' +
                    '<p> <span class="offer-main">Quantity</span>' + value.quantity + '</p>' +
                    '</div>' +
                    '<div></div> ' +
                    '<div>' +
                    '<p> <span class="offer-main">Date</span>' +
                    '<span class="offer-date">' +
                    '<span class="offer-date-p"> Placed</span>' +
                    '<span class="hidden-above-1120">:</span>' +
                    '<br class="hidden-above-1120">' + moment(data.createdAt).format("MMMM DD, YYYY") +
                    '<br>' +
                    '<span class="offer-date-p"> Fulfilled</span>' +
                    '<span class="hidden-above-1120">:</span>' +
                    '<br class="hidden-above-1120"> ' + moment(data.deliveryTime).format("MMMM DD, YYYY") + '</span>' +
                    '</p>' +
                    '</div>' +
                    '</div>' +
                    '<div class="t-right">' + cancelBtn +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

                $('#orderListing').append(listingData);

            });
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
});

//Search result
$(".searchBtn").click(function () {

    var search = $("#searchText").val().trim();
    var result = '';
    if (search == '') {
        swal({
                title: "",
                text: "Please Enter confirmation Code !",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
            function () {});
    } else {
        //Call API with where condition of orderNumber
        $.ajax({
            url: base_url + 'orders?filter={"where" : {"orderNumber" : "' + search + '"}}',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    window.location = 'view-receipt?id=' + search
                } else {
                    swal({
                            title: "",
                            text: "Invalid Confirmation Code !",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                        function () {
                            // $("#searchText").focus();
                        });
                }
            },
            error: function (response) {
                console.log(response);
                if (response.status == 401) {
                    swal({
                            title: "",
                            text: errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false
                        },
                        function () {
                            localStorage.removeItem("superUserId");
                            window.location = 'login';
                        });
                }
            }
        });
    }
});