/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}


$(document).ready(function () {

    $.ajax({
        // url: base_url + 'orders?filter={"include":["account",{"orderdetails":["product"]}],"where":{"orderNumber":"' + orderNo + '"}}',
        url: base_url + 'orders?filter={"where": {"orderNumber": "' + params + '"}, "include":["establishment",{"orderdetails":["product"]}]}',
        success: function (data) {
            data = data[0];
            console.log(data);

            $('.orderNo').html(params);
            $('#viewReceipt').attr('href', 'view-receipt?id=' + params);

            if (data.status == 0 || data.status == 1) {
                status = 'Processing';
            }
            else if (data.status == 2) {
                status = 'Fulfilled';
            }
            else if (data.status == 3) {
                status = 'Rejected';
            }
            else if (data.status == 4) {
                status = 'Cancelled';
            }

            $.each(data.orderdetails, function (key, value) {

                var listingData =
                    '<div class="row">'
                    + '<div class="col-sm-12">'
                    + '<div class="card card-1">'
                    + '<div>'
                    + '<img src="images/bottle.png" alt="">'
                    + '</div>'
                    + '<div class="product-info-1">'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Product</span>' + value.product.name + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Bottles Sold</span> 20/500</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">UPC Code</span>' + value.product.upc + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Revenue</span> $530.9</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Item Category</span>' + value.product.category + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Total Discount</span> $253.1</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Original Price</span> $' + value.actualPrice.toFixed(2) + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Bevvi Revenue</span> NA</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Discount Price</span> $' + value.price.toFixed(2) + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Sales Tax</span> NA</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Quantity</span>' + value.quantity + ' </p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Order Number</span>'
                    + '<span class="col-primary">' + data.orderNumber + '</span>'
                    + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Date</span>'
                    + '<span class="offer-date">'
                    + '<span class="offer-date-p2">Start</span>'
                    + '<span class="hidden-above-1120">:</span>'
                    + '<br class="hidden-above-1120"> ' + moment(data.createdAt).format("MMMM DD, YYYY") +
                    + '<br>'
                    + '<span class="offer-date-p2">End</span>'
                    + '<span class="hidden-above-1120">:</span>'
                    + '<br class="hidden-above-1120">' + moment(data.deliveryTime).format("MMMM DD, YYYY") + '</span>'
                    + '</p>'
                    + '</div>'
                    + '<div>'
                    + '<p>'
                    + '<span class="offer-main">Status</span>'
                    + '<span class="col-primary">'+status+'</span>'
                    + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="t-right">'
                    + '<a href="">Cancel</a>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    ;




                // <div class="row mar-top40">' +
                //     '<div class="col-sm-12">' +
                //     '<div class="card card-1">' +
                //     '<div>' +
                //     '<img src="images/bottle.png" alt="">' +
                //     '</div>' +
                //     '<div class="product-info-1">' +
                //     '<div>' +
                //     '<p><span class="offer-main">Product</span>' + value.product.name + '</p>' +
                //     '</div>' +
                //     '<div>' +
                //     '<p class="status"><span class="offer-main">Status</span>' + status + '</p>' +
                //     '</div>' +
                //     '<div>' +
                //     '<p><span class="offer-main">UPC Code</span>' + value.product.upc + '</p>' +
                //     '</div>' +
                //     '<div>' +
                //     '<p> <span class="offer-main">Order number</span>' + data.orderNumber + '</p> ' +
                //     '</div>' +
                //     '<div>' +
                //     '<p> <span class="offer-main">Item Category</span>' + value.product.category + '</p> ' +
                //     '</div>' +
                //     '<div></div> ' +
                //     '<div>' +
                //     '<p> <span class="offer-main">Original Price</span> $' + value.actualPrice.toFixed(2) + '</p>' +
                //     '</div> ' +
                //     '<div></div> ' +
                //     '<div> ' +
                //     '<p> <span class="offer-main">Discount Price</span> $' + value.price.toFixed(2) + '</p>' +
                //     '</div>' +
                //     '<div></div> ' +
                //     '<div>' +
                //     '<p> <span class="offer-main">Quantity</span>' + value.quantity + '</p>' +
                //     '</div>' +
                //     '<div></div> ' +
                //     '<div>' +
                //     '<p> <span class="offer-main">Date</span>' +
                //     '<span class="offer-date">' +
                //     '<span class="offer-date-p"> Placed</span>' +
                //     '<span class="hidden-above-1120">:</span>' +
                //     '<br class="hidden-above-1120">' + moment(data.createdAt).format("MMMM DD, YYYY") +
                //     '<br>' +
                //     '<span class="offer-date-p"> Fulfilled</span>' +
                //     '<span class="hidden-above-1120">:</span>' +
                //     '<br class="hidden-above-1120"> ' + moment(data.deliveryTime).format("MMMM DD, YYYY") + '</span>' +
                //     '</p>' +
                //     '</div>' +
                //     '</div>' +
                //     '<div class="t-right">' +
                //     '<a href="">Cancel</a>' +
                //     '</div>' +
                //     '</div>' +
                //     '</div>' +
                //     '</div>';

                $('#orderListing').append(listingData);

            });
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
});


//Search result
$(".searchBtn").click(function () {

    var search = $("#searchText").val();
    var result = '';
    if (search == '') {
        swal("Error","Please Enter confirmation Code !","Error");
    }
    else {
        //Call API with where condition of orderNumber
        $.ajax({
            url: base_url + 'orders?filter={"where" : {"orderNumber" : "' + search + '"}}',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    window.location = 'view-receipt?id=' + search
                }
            }
        });
    }
});