/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//include header file
$("#header").load("includes/header", function () {
    loadDataIntoHeader();
});

/*  GET PARAMETER FROM URL */
var url = window.location.href;
var curpag = url.split('/')[4];
var new_Ar = curpag.split('?');
if (new_Ar.length > 1) {
    var params = new_Ar[1].split('=')[1];
}

/*  GET PARAMETER FROM URL */
console.log(url.split('/'));
//set base url
if (url.split('/')[2] === 'localhost' || url.split('/')[2] === 'localhost:8081' || url.split('/')[2] === "dev.business.getbevvi.com") {
    var base_url = 'http://dev.getbevvi.com:4000/api/';
} else {
    var base_url = 'https://getbevvi.com:4000/api/';
}

var access_token = localStorage.getItem('accessToken');
var errorMessage = "You have been logged out. Please log back in.";
var dateErrorMessage = "Start date should be smaller than end date";

/*****************************Logout***********************************************/
$(document).on('click', '.signout', function () {
    $.post({
        url: base_url + 'superadmins/logout?access_token=' + access_token,
        success: function () { },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        },
        complete: function (xhr) {
            if (xhr.status === 204) {
                /*clear localStorage variable on click of logout. */
                //localStorage.clear();
                localStorage.removeItem("superUserId");
                window.location = 'login';
            }
        }
    });
});
/*****************************Logout***********************************************/

/*****************************Page Active indicator***********************************************/
$(document).ready(function () {
    loadDataIntoHeader();
});

function loadDataIntoHeader() {
    var url = window.location.href;
    var curpag = url.split('/')[4];

    var new_Ar = curpag.split('?');
    var curpage = new_Ar[0];

    if (new_Ar.length > 1) {
        var params = new_Ar[1].split('=')[1];
    }
    if (curpage === 'establishment' || curpage === 'account') {
        $('aside').addClass('hide');
    }
    if (curpage === 'store-transaction') {
        curpage = 'reports'
    }
    if (curpage === 'view-orders' || curpage === 'live-orders' || curpage === 'view-receipt') {
        curpage = 'manage-orders';
    } else if (curpage === 'transaction-details') {
        curpage = 'transactions';
    } else if (curpage === 'add-promotion') {
        curpage = 'promotions';
    } else if (curpage === 'add-account') {
        curpage = 'setting';
    }
    //alert(curpage);
    $("#sidenav a").each(function () {
        var page = $(this).attr('href');
        if (curpage === page) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });

    // var username = localStorage.getItem('username');
    // var shopname = localStorage.getItem('shopname');
    // if (username) {
    //     $('.dropdown-toggle span').text(username);
    // }
    // if (shopname) {
    //     $('.navbar-brand').html(shopname);
    // }
}
//Restrict right click and console
// document.onkeydown = function (e) {
//    if (event.keyCode == 123) {
//        return false;
//    }
//    if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
//        return false;
//    }
//    if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
//        return false;
//    }
//    if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
//     return false;
// }
//    if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
//        return false;
//    }
// }
// $(document).ready(function () {
//    $(document).bind("contextmenu", function (e) {
//        return false;
//    });
// });


/***************************Push Notification*************************************************/


// Initialize Firebase
var config = {
    apiKey: "AIzaSyAtqhpavJIdPUdx_FEky7mXaOOD78vQrHU",
    authDomain: "bevvi-495d6.firebaseapp.com",
    databaseURL: "https://bevvi-495d6.firebaseio.com",
    projectId: "bevvi-495d6",
    storageBucket: "bevvi-495d6.appspot.com",
    messagingSenderId: "263058269693"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.onMessage(function (payload) {
    console.log("payload :: ", payload);
    var instance = JSON.parse(payload.data.instance);
    console.log("payload :: ", );
    //    notification = [];
    //    notification.push(payload);
    //    localStorage.setItem('notification', notification);
    //    var notificationdata = localStorage.getItem('notification');
    //    $.each(notificationdata, function (i, noti) {
    //        console.log(noti);
    if (payload.data.type === "ORDER_CREATED") {
        $(document).on('click', '.order-alert-btn', function () {
            $('section.order-alert1').addClass('hide');
            location.reload();
        });
        $('section.order-alert1 .order_number span').text(instance.orderNum);
        $('section.order-alert1 h3.timing').text(moment(instance.deliveryTime).format('MMM DD, hh:mm A'));
        $('section.order-alert1').removeClass('hide');
    } else if (payload.data.type === "Scheduled Order Placed") {
        $(document).on('click', '.order-alert-btn2', function () {
            $('section.order-alert2').addClass('hide');
        });
        $('section.order-alert2 .order_number span').text(payload.data.orderid);
        $('section.order-alert2 h3.timing').text("Delivery at " + moment(payload.data.time, 'DD-MM-YYYY, hh:mm A').format('hh:mm A'));
        $('section.order-alert2').removeClass('hide');
    } else if (payload.data.type === "Order Canceled") {
        $(document).on('click', '.cancel-alert-btn', function () {
            $('section.cancel-alert1').addClass('hide');
        });
        $('section.cancel-alert1 .order_number span').text(payload.data.orderid);
        $('section.cancel-alert1').removeClass('hide');
    }
    //    });



    //    console.log('onMessage',body_data);

});


/* Sort Function */
function sortResults(ary, prop, asc) {

    ary = ary.sort(function (a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    console.log(ary);
    return ary;
}