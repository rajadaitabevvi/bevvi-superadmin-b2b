/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

///////////// global variables.
var liveOrders = Array();


$(document).ready(function () {
    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");

    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format('MM/DD/YYYY');
        var endDate = moment().endOf('month').format('MM/DD/YYYY');
        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    } else {
        var startDate = sdate;
        var endDate = edate;
        $(".startDate").val(sdate);
        $(".endDate").val(edate);
    }

    var sdate = moment().startOf('month').format('L');
    var edate = moment().format('L');

    loadForm(startDate, endDate);

    //Suggest by Raja
    $("#searchText").attr("disabled", 'disabled');
    //$("#searchBtn").attr("readonly", "readonly");

});

$("#searchBtn").click(function () {

});

function loadOrders(establishmentId) {

    var filterOrders = $.grep(liveOrders, function (i, value) {
        return i[0].id == establishmentId;
    });

    $("#rtp_orders").html('');
    $("#orders").html('');
    filterOrders = filterOrders[0][0];

    openOrder = $.grep(filterOrders.orders, function (i, e) {
        return i.status == 0;
    });
    openOrder = sortResults(openOrder, 'deliveryTime', true);
    if (openOrder.length > 0) {
        $.each(openOrder, function (key, value) {
            if (key < 5) {
                var orderList = '<tr>' +
                    '<td>' +
                    '<span class="orderNo" data="' + value.orderNumber + '">' + value.orderNumber + '</span>' +
                    '<br>' +
                    '<span class="qty">' + value.qty + ' items for ' + value.account.firstName + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<span class="time">' + moment(value.deliveryTime).toNow(true) + '</span>' +
                    '</td>' +
                    '</tr >';
                $("#orders").append(orderList);
            }
        });
    } else {
        var orderList = '<tr>' +
            '<td>' + 'No data found !' + '</td>' +
            '</tr >';
        $("#orders").append(orderList);
    }


    rtp = $.grep(filterOrders.orders, function (i, e) {
        return i.status == 1;
    });

    //console.log(rtp);
    if (rtp.length > 0) {
        rtp = sortResults(rtp, 'updatedAt', false);
        $.each(rtp, function (key, value) {
            if (key < 5) {
                var orderList = '<tr>' +
                    '<td>' +
                    '<span class="orderNo" data="' + value.orderNumber + '">' + value.orderNumber + '</span>' +
                    '<br>' +
                    '<span class="qty">' + value.qty + ' items for ' + value.account.firstName + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<span class="time">' + moment(value.deliveryTime).format('hh:mm a') + '</span>' +
                    '</td>' +
                    '</tr>';
                $("#rtp_orders").append(orderList);
            }
        });
    } else {
        var orderList = '<tr>' +
            '<td>' + 'No data found !' + '</td>' +
            '</tr>';
        $("#rtp_orders").append(orderList);

    }
}


////////////////////////////// for dropdown change event

$('#live-orders').change(function () {
    var value = $(this).val();
    loadOrders(value);
});

//on order click
$(".orderlist .orderNo").click(function () {
    //alert("12");
    //alert($(this).text());
});

///////////////////////////////////////for date filter
$('.date').change(function () {

    var sDate = $('#startDate').val();
    var eDate = $('#endDate').val();

    var startDate = moment(sDate).format('MM/DD/YYYY');
    var endDate = moment(eDate).format('MM/DD/YYYY');
    localStorage.setItem("start_date", startDate);
    localStorage.setItem("end_date", endDate);

    // loadForm(startDate, endDate);
    if (moment(startDate, 'MM/DD/YYYY') > moment(endDate, 'MM/DD/YYYY')) {
        swal({
            title: "",
            text: dateErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function () {
                startDate = moment().startOf('month').format("MM/DD/YYYY");
                endDate = moment().endOf('month').format("MM/DD/YYYY");
                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                loadForm(startDate, endDate);
            });
    } else {
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
        loadForm(startDate, endDate);
    }
});


function loadForm(startDate, endDate) {
    console.log("sanchit")
    ////////////////////////////// for get count and category   
    $.ajax({
        url: base_url + "superadmins/mainReport?startDate=" + startDate + "&endDate=" + endDate + "&access_token=" + access_token,
        success: function (result) {
            $('#partnerCount').html(result.partnerCount);
            $('#revenue').html("$" + result.revenue.toFixed(2));
            $('#bevviCommission').html("$" + result.bevviCommission.toFixed(2));
            $('#Liquor').html(result.category.Liquor == null ? 0 + " %" : result.category.Liquor + " %");
            $('#Wine').html(result.category.Wine == null ? 0 + "%" : result.category.Wine + "%");
            $('#Beer').html(result.category.Beer == null ? 0 + "%" : result.category.Beer + "%");

            $('#startDate').datepicker().val(moment(startDate).format('MM/DD/YYYY'));
            $('#endDate').datepicker().val(moment(endDate).format('MM/DD/YYYY'));
            $('.date').html(moment(startDate).format('MM/DD') + ' - ' + moment(endDate).format('MM/DD'));
        },
        error: function (response) {
            //console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });


    /////////////////////////////  for live orders 
    $.ajax({
        url: base_url + "superadmins/liveOrders",
        data: {
            "startDate": startDate,
            "endDate": endDate,
            "access_token": access_token
        },
        success: function (data) {
            liveOrders = data;
            console.log('liveOrders');
            console.log(liveOrders);
            if (liveOrders.length > 0) {
                $("#live-orders").html(''); // Clear Dropdown
                $.each(data, function (i, value) {
                    value = value[0];
                    if (i == 0) {
                        $("#live-orders").append($('<option selected value=' + value.id + '>' + value.name + '</option>'));
                    } else {
                        $("#live-orders").append($('<option value=' + value.id + '>' + value.name + '</option>'));
                    }
                    loadOrders(value.id);
                });
                $('#live-orders').trigger("change");
            } else {
                $("#live-orders").append($('<option selected disabled value="">' + 'No Data' + '</option>'));
                $("#orders").append('<tr><td>No data found !</td></tr >');
                $("#rtp_orders").append('<tr><td>No data found !</td></tr >');
                return false;
            }
        },
        error: function (response) {
            //console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
}

setInterval(function () {
    location.reload();
}, 120000); // 1min = 60000 
