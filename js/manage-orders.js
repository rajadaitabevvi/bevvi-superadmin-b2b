/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

orders = Array();
CancelledOrders = Array();

$(document).ready(function () {
    //Date picker
    $(".datepicker-head").datepicker({
        dateFormat: 'mm/dd/yy'
    });

    startDate = localStorage.getItem("start_date");
    endDate = localStorage.getItem("end_date");
    if (startDate == null || endDate == null) {
        var startDate = moment().startOf('month').format('MM/DD/YYYY');
        var endDate = moment().endOf('month').format('MM/DD/YYYY');
        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    } else {
        var startDate = startDate;
        var endDate = endDate;
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    }

    GetDataApi(startDate, endDate);
    // sDate = localStorage.getItem("StartDate");
    // eDate = localStorage.getItem("EndDate");

    // if (sDate == null || eDate == null) {
    //     startDate = moment().startOf('month').format("MM/DD/YYYY");
    //     endDate = moment().format("MM/DD/YYYY");
    //     localStorage.setItem("StartDate", startDate);
    //     localStorage.setItem("EndDate", endDate);
    //     $("#startDate").val(startDate);
    //     $("#endDate").val(endDate);
    // }
    // else {
    //     $("#startDate").val(sDate);
    //     $("#endDate").val(eDate);
    // }



});

function GetDataApi(startDate, endDate) {
    $.ajax({ //Live orders
        url: base_url + 'superadmins/orderStoreList',
        data: {
            startDate: startDate,
            endDate: endDate,
            status: 1,
            access_token: localStorage.getItem("accessToken")
        },
        dataType: "json",
        success: function (data) {
            orders = sortResults(data, 'name', true);            
            console.log(orders);
            if (data.length < 2) {
                $("#sortBy").attr("disabled", "disabled");
            }
            loadData(orders, 1);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

    $.ajax({ //Cancelled orders
        url: base_url + 'superadmins/orderStoreList',
        data: {
            startDate: startDate,
            endDate: endDate,
            status: 4,
            access_token: localStorage.getItem("accessToken")
        },
        dataType: "json",
        success: function (data) {
            CancelledOrders = sortResults(data, 'name', true);            
            console.log(CancelledOrders);
            if (data.length < 2) {
                $("#sortBy").attr("disabled", "disabled");
            } else {
                $("#sortBy").attr("disabled", false);
            }
            loadData(CancelledOrders);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
}


function loadData(orders, status) {

    if (status == 1) {
        $(".liveOrders tbody").html('');
        if (orders.length > 0) {
            $.each(orders, function (key, value) {
                liveOrderList = '<tr>' +
                    '<td>' +
                    value.name +
                    '</td>' +
                    '<td>' +
                    '<a href="view-orders?id=' + value.id + '">' + value.ordercount + ' orders</a>' +
                    '</td>' +
                    '<td>NA' +
                    '</td>' +
                    '<td>$' + value.charge.toFixed(2) + '</td>' +
                    '</tr>';
                $(".liveOrders tbody").append(liveOrderList);
            });
        } else {
            $(".liveOrders tbody").append('<tr><td colspan="4" style="text-align:center" >No Data Found !</td></tr>');
        }


    } else {
        $(".cancelledOrders tbody").html('');
        if (CancelledOrders.length > 0) {
            $.each(CancelledOrders, function (key, value) {
                cancelOrderList = '<tr>' +
                    '<td>' +
                    value.name +
                    '</td>' +
                    '<td>' +
                    '<a href="view-orders?id=' + value.id + '">' + value.ordercount + ' orders</a>' +
                    '</td>' +
                    '<td class="col-primary">' + 'NA' + '</td > ' +
                    '<td class="col-danger">- $' + value.charge.toFixed(2) + '</td>' +
                    '</tr>';
                $(".cancelledOrders tbody").append(cancelOrderList);
            });
        } else {
            $(".cancelledOrders tbody").append('<tr><td colspan="4" style="text-align:center" >No Data Found !</td></tr>');
        }
    }
}

//Sort Orders
$("#sortBy").on('change', function () {
    sort = $(this).val();

    if (sort == 'asc') {
        order = sortResults(orders, 'name', true);
        loadData(order, 1);
        CancelledOrders = sortResults(CancelledOrders, 'name', true);
        loadData(CancelledOrders);
    } else {
        order = sortResults(orders, 'name', false);
        loadData(order, 1);
        CancelledOrders = sortResults(CancelledOrders, 'name', false);
        loadData(CancelledOrders);
    }
});

///Start Date & End Date Change
$(".datepicker-head").on("change", function () {
    sDate = $("#startDate").val();
    eDate = $("#endDate").val();
    localStorage.setItem("start_date", sDate);
    localStorage.setItem("end_date", eDate);

    if (sDate > eDate) {
        $("#startDate").val(moment().startOf('month').format('MM/DD/YYYY'));
        $("#endDate").val(moment().endOf('month').format('MM/DD/YYYY'));
        //alert("start date should be small then end date");
        swal({
                title: "",
                text: dateErrorMessage,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
            function () {
                // $("#searchText").focus();
                sDate = $("#startDate").val();
                eDate = $("#endDate").val();
                GetDataApi(sDate, eDate);
            });
    } else {
        sDate = $("#startDate").val();
        eDate = $("#endDate").val();
        GetDataApi(sDate, eDate);
    }
});


//Search result
$(".searchBtn").click(function () {

    var search = $("#searchText").val().trim();
    var result = '';
    if (search == '') {
        swal({
            title: "",
            text: "Please Enter confirmation Code !",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
        function () {
           // $("#searchText").focus();
        });
    } else {
        //Call API with where condition of orderNumber
        $.ajax({
            url: base_url + 'orders?filter={"where" : {"orderNumber" : "' + search + '"}}',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    window.location = 'view-receipt?id=' + search
                }
                else{
                    swal({
                        title: "",
                        text: "Invalid Confirmation Code !",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                    function () {
                       // $("#searchText").focus();
                    });
                }
            },
            error: function (response) {
                console.log(response);
                if (response.status == 401) {
                    swal({
                            title: "",
                            text: errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false
                        },
                        function () {
                            localStorage.removeItem("superUserId");
                            window.location = 'login';
                        });
                }
            }
        });
    }
});