/* Authorization */
/* global base_url */

var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

$(document).ready(function () {
    sort('asc');
});

//Accept number only in input
jQuery('.number').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

///// change Event
$('#sortBy').change(function () {
    var val = $('#sortBy').val();
    sort(val);
});

//set page to come back to this page when click on add store
$(".add-store a").click(function () {
    localStorage.setItem("backToPage", "setting");
});

//Open Dialog popup of add commision
function openPopup(e, storeName) {
    $("#AddCommissionBtn").attr('data-id', e);

    $.getJSON(base_url + 'commissions?filter={"where":{"bizAccountId":"' + e + '"}}', function (result) {

        if (result.length) {
            result = result[0];
            chargePerUnit = $("#chargePerUnit").val(result.chargePerUnit);
            tier1_revenue = $("#tier1_revenue").val(result.tiers.tier1.revenue);
            tier1_fee = $("#tier1_fee").val(result.tiers.tier1.fee);
            tier2_revenue = $("#tier2_revenue").val(result.tiers.tier2.revenue);
            tier2_fee = $("#tier2_fee").val(result.tiers.tier2.fee);
            $("#myModal-commission h3").text(storeName);
            $("#myModal-commission").modal('show');
        } else {
            chargePerUnit = $("#chargePerUnit").val('');
            tier1_revenue = $("#tier1_revenue").val('');
            tier1_fee = $("#tier1_fee").val('');
            tier2_revenue = $("#tier2_revenue").val('');
            tier2_fee = $("#tier2_fee").val('');
            $("#myModal-commission h3").text(storeName);
            $("#myModal-commission").modal('show');
        }
    });
}

//Store add Commission
$("#AddCommissionBtn").click(function () {
    bizId = $(this).attr('data-id');
    chargePerUnit = $("#chargePerUnit").val();
    tier1_revenue = $("#tier1_revenue").val();
    tier1_fee = $("#tier1_fee").val();
    tier2_revenue = $("#tier2_revenue").val();
    tier2_fee = $("#tier2_fee").val();
    if (chargePerUnit === null || chargePerUnit === '') {
        chargePerUnit = 0;
    }
    if (tier1_revenue === null || tier1_revenue === '') {
        tier1_revenue = 0;
    }
    if (tier1_fee === null || tier1_fee === '') {
        tier1_fee = 0;
    }
    if (tier2_revenue === null || tier2_revenue === '') {
        tier2_revenue = 0;
    }
    if (tier2_fee === null || tier2_fee === '') {
        tier2_fee = 0;
    }


    $.ajax({
        url: base_url + 'commissions/upsertWithWhere?where={ "bizAccountId" : "' + bizId + '"}',
        data: {
            bizAccountId: bizId,
            chargePerUnit: chargePerUnit,
            tiers: {
                tier1: {
                    revenue: tier1_revenue,
                    fee: tier1_fee
                },
                tier2: {
                    revenue: tier2_revenue,
                    fee: tier2_fee
                }
            }
        },
        type: "POST",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#myModal-commission").modal('hide');
        }
    });
});

function sort(val) {
    $.ajax({
        url: base_url + "establishments/",
        data: 'filter={"where":{"bevviActive": true}, "include":["bizaccount"]}',
        success: function (data) {
            data = sortResults(data, 'name', val);
            console.log(data);
            $('#listingData').html('');
            $.each(data, function (key, value) {

                if (value.url) {
                    website = value.url;
                } else {
                    website = 'NA';
                }
                if (value.phoneNum) {
                    phone = value.phoneNum;
                } else {
                    phone = 'NA';
                }
                if (value.email) {
                    email = value.email;
                } else {
                    email = 'NA';
                }

                var listingData = '<tr>' +
                        '<td><a onclick="openPopup(\'' + value.bizAccountId.toString() + '\',\'' + value.name + '\')" >Add<br> Commission</a></td>' +
                        '<td><a href="store-detail?id=' + value.id + '">' + value.name + '</a></td>' +
                        '<td width="10%">' + value.address + '</td>' +
                        '<td>' + phone + '</td>' +
                        '<td>' + email + '</td>' +
                        '<td>' + website + '</td>' +
                        '</tr>';
                $('#listingData').append(listingData);

                // if (key ==== 5) {
                //     return false;
                // }
            });
        },
        error: function (response) {
            console.log(response);
            if (response.status === 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                        function () {
                            localStorage.removeItem("superUserId");
                            window.location = 'login';
                        });
            }
        }
    });
}

// For sorting /
function sortResults(ary, prop, asc) {
    ary = ary.sort(function (a, b) {
        if (asc === 'asc') {
            return (a[prop].toLowerCase() > b[prop].toLowerCase()) ? 1 : ((a[prop].toLowerCase() < b[prop].toLowerCase()) ? -1 : 0);
        } else {
            return (b[prop].toLowerCase() > a[prop].toLowerCase()) ? 1 : ((b[prop].toLowerCase() < a[prop].toLowerCase()) ? -1 : 0);
        }
    });
    return ary;
}