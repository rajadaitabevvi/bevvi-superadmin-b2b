/////////////////// global variables.
//var liveOrders = [];
var establishmentId = params;
$(document).ready(function () {

    // if (localStorage.getItem("RS_date") != '' && localStorage.getItem("RE_date") != '') {
    //     startDate = localStorage.getItem("RS_date");
    //     endDate = localStorage.getItem("RE_date");
    // } else {
    //     var startDate = moment().startOf('month').format('YYYY-MM-DD');
    //     var endDate = moment().format('YYYY-MM-DD');
    // }
    sdate = localStorage.getItem("start_date");
    edate = localStorage.getItem("end_date");
    if (sdate == null || edate == null) {
        var startDate = moment().startOf('month').format('MM/DD/YYYY');
        var endDate = moment().endOf('month').format('MM/DD/YYYY');
        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);
        $(".startDate").val(startDate);
        $(".endDate").val(endDate);
    } else {
        var startDate = sdate;
        var endDate = edate;
        $(".startDate").val(sdate);
        $(".endDate").val(edate);
    }

    // var startDate = moment().startOf('month').format('YYYY-MM-DD');
    // var endDate = moment().endOf('month').format('YYYY-MM-DD');

    // var sdate = moment().startOf('month').format('L');
    // var edate = moment().endOf('month').format('L');

    $.ajax({
        url: base_url + "establishments/" + establishmentId,
        success: function (data) {
            $('#storeName').html(data.name);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

    loadData(startDate, endDate);

    $('.date').change(function () {

        var startDate = moment($('#startDate').val()).format('YYYY-MM-DD');
        var endDate = moment($('#endDate').val()).format('YYYY-MM-DD');

        localStorage.setItem("start_date", startDate);
        localStorage.setItem("end_date", endDate);

        startDate = localStorage.getItem("start_date");
        endDate = localStorage.getItem("end_date");

        loadData(startDate, endDate);
    });

    $('#printTransactions').click(function () {
        PrintElem('printableArea');


    });
});

function loadData(startDate, endDate) {

    $.ajax({
        url: base_url + "establishments/offers?establishmentId=" + establishmentId + "&startDate=" + startDate + "&endDate=" + endDate + "&access_token=" + access_token,

        success: function (data) {
            val = 0;
            $('#startDate').datepicker().val(moment(startDate).format('MM/DD/YYYY'));
            $('#endDate').datepicker().val(moment(endDate).format('MM/DD/YYYY'));

            $('#liveData').html('');
            $('#pastOrder').html('');

            ///Header value set // Not getting array from api
            $('#date').html(moment($('#startDate').val()).format('MM/DD') + '-' + moment($('#endDate').val()).format('MM/DD'));
            $("#status").html("NA");
            $('#start').html(moment($('#startDate').val()).format('MMM DD'));
            $('#end').html(moment($('#endDate').val()).format('MMM DD'));
            $('#dayLeft').html('NA');
            $('#fulfilled').html('NA');

            //liveorders
            if ($.isEmptyObject(data.liveOffers) == false) {
                ///Load Live Offers
                $.each(data.liveOffers, function (key, value) {
                    var paid = value.revenue + value.tax;
                    if (value.bevviRevenue > 0) {
                        var debit_credit = 'Credit <br> $' + value.bevviRevenue.toFixed(2);
                    } else {
                        var debit_credit = 'Debit <br> $' + value.bevviRevenue.toFixed(2);
                    }
                    $('#liveData').append('<tr>' +
                        // '<td>' + value.offer.name + '</td>' +
                        '<td>' + value.offer.product.name + '</td>' +
                        // '<td>' + (value.offer.totalQty - value.offer.remQty) + '/' + value.offer.totalQty + '</td>' +
                        '<td>' + (value.qtySold) + '/' + value.offer.totalQty + '</td>' +
                        '<td>$' + value.revenue.toFixed(2) + '</td>' +
                        '<td>$' + value.discount.toFixed(2) + '</td>' +
                        '<td>$' + paid.toFixed(2) + '</td>' +
                        '<td>$' + value.commission.toFixed(2) + '</td>' +
                        '<td>' + debit_credit + '</td>' +
                        '<td>$' + value.discount.toFixed(2) + '</td>' +
                        '</tr>');
                });
            } else {
                $('#liveData').html('<tr><td colspan="8" style="text-align:center;">No Live Offers !</td></tr>');
            }

            ///live Total
            if ($.isEmptyObject(data.livetotal) == false) {
                ////////// To load Live total
                $('#liveTotal_fulfilled').html(data.livetotal.qtySold + '/' + data.livetotal.totalQty);
                $('#liveTotal_revenue').html('$' + data.livetotal.revenue.toFixed(2));
                $('#liveTotal_promotion').html('$' + data.livetotal.discount.toFixed(2));
                $('#liveTotal_paid').html('$' + data.livetotal.paid.toFixed(2));
                $('#liveTotal_commission').html('$' + data.livetotal.commission.toFixed(2));
                if (data.livetotal.credit > 0) {
                    $('#live_credit_toshow').show();
                    $('#liveTotal_credit').html('$' + data.livetotal.credit);
                } else {
                    $('#live_credit_toshow').hide();
                }
                if (data.livetotal.debit < 0) {
                    $('#live_debit_toshow').show();
                    $('#liveTotal_debit').html('$' + data.livetotal.debit);
                } else {
                    $('#live_debit_toshow').hide();
                }
                $('#liveTotal_discount').html('$' + data.livetotal.discount.toFixed(2));
            } else {
                ////////// To load Live total
                $('#liveTotal_fulfilled').html('$ NA');
                $('#liveTotal_revenue').html('$' + val.toFixed(2));
                $('#liveTotal_promotion').html('$' + val.toFixed(2));
                $('#liveTotal_paid').html('$' + val.toFixed(2));
                $('#liveTotal_commission').html('$' + val.toFixed(2));
                $('#live_credit_toshow').hide();
                $('#live_debit_toshow').hide();
                $('#liveTotal_discount').html('$' + val.toFixed(2));
            }

            // console.log('data.pastOffers');
            // console.log(data.pastOffers);
            if ($.isEmptyObject(data.pastOffers) == false) {
                ///////// To load past offers
                $.each(data.pastOffers, function (key, value) {
                    var paid = value.revenue + value.tax;
                    if (value.bevviRevenue > 0) {
                        var debit_credit = 'Credit <br> $' + value.bevviRevenue.toFixed(2);
                    } else {
                        var debit_credit = 'Debit <br> $' + value.bevviRevenue.toFixed(2);
                    }
                    var data1 = '<tr>' +
                        '<td><span>Start:</span>' + moment(startDate).format("MM/DD") + '<br>' +
                        '<span>End:</span>' + moment(endDate).format("MM/DD") + '</td>' +
                        // '<td>' + (value.offer.totalQty - value.offer.remQty) + '/' + value.offer.totalQty + '</td>' +
                        '<td>' + (value.qtySold) + '/' + value.offer.totalQty + '</td>' +
                        '<td>$' + value.revenue.toFixed(2) + '</td>' +
                        '<td>$' + value.discount.toFixed(2) + '</td>' +
                        '<td>$' + paid.toFixed(2) + '</td>' +
                        '<td>$' + value.commission.toFixed(2) + '</td>' +
                        '<td>' + debit_credit + '</td>' +
                        '<td>$' + value.discount.toFixed(2) + '</td>' +
                        '</tr>';
                    $('#pastOrder').append(data1);
                });
            } else {
                $('#pastOrder').html('<tr><td colspan="8" style="text-align:center;">No Past Offers !</td></tr>');
            }

            // console.log('data.pasttotal');
            // console.log(data.pasttotal);
            if ($.isEmptyObject(data.pasttotal) == false) {
                if (data.pasttotal.bevviRevenue > 0) {
                    var debit_credit = 'Credit <br> $' + data.pasttotal.bevviRevenue.toFixed(2);
                } else {
                    var debit_credit = 'Debit <br> $' + data.pasttotal.bevviRevenue.toFixed(2);
                }
                //////// To load past total
                $('#pastTotal_fulfilled').html(data.pasttotal.qtySold + '/' + data.pasttotal.totalQty);
                $('#pastTotal_revenue').html('$' + data.pasttotal.revenue.toFixed(2));
                $('#pastTotal_promotion').html('$' + data.pasttotal.discount.toFixed(2));
                $('#pastTotal_paid').html('$' + data.pasttotal.paid.toFixed(2));
                $('#pastTotal_commission').html('$' + data.pasttotal.commission.toFixed(2));
                if (data.pasttotal.credit > 0) {
                    $('#past_credit_toshow').show();
                    $('#pastTotal_credit').html('$' + data.pasttotal.credit);
                } else {
                    $('#past_credit_toshow').hide();
                }
                if (data.pasttotal.debit < 0) {
                    $('#past_debit_toshow').show();
                    $('#pastTotal_debit').html('$' + data.pasttotal.debit);
                } else {
                    $('#past_debit_toshow').hide();
                }
                $('#pastTotal_discount').html('$' + data.pasttotal.discount.toFixed(2));
            } else {
                //////// To load past total
                $('#pastTotal_fulfilled').html('$ NA');
                $('#pastTotal_revenue').html('$' + val.toFixed(2));
                $('#pastTotal_promotion').html('$' + val.toFixed(2));
                $('#pastTotal_paid').html('$' + val.toFixed(2));
                $('#pastTotal_commission').html('$' + val.toFixed(2));
                $('#past_credit_toshow').hide();
                $('#past_debit_toshow').hide();
                $('#pastTotal_discount').html('$' + val.toFixed(2));
            }

            console.log('grand total');
            console.log(data);
            // //Load Grand Total div
            // $('#grandTotal_fulfilled').html('$ NA');
            // $('#grandTotal_revenue').html('$' + data.grandtotal.bevviRevenue.toFixed(2));
            // $('#grandTotal_promotion').html('$' + data.grandtotal.discount.toFixed(2));
            // $('#grandTotal_paid').html('$' + data.grandtotal.paid.toFixed(2));
            // $('#grandTotal_commission').html('$' + data.grandtotal.bevviRevenue.toFixed(2));
            // $('#grandTotal_debit').html('$ NA');
            // $('#grandTotal_credit').html('$ NA');
            // $('#grandTotal_discount').html('$' + data.grandtotal.discount.toFixed(2));

            // console.log('data.grandtotal');
            // console.log(data.grandtotal);
            if ($.isEmptyObject(data.grandtotal) == false) {
                //Load Grand Total div
                $('#grandTotal_fulfilled').html(data.grandtotal.qtySold + '/' + data.grandtotal.totalQty);
                $('#grandTotal_revenue').html('$' + data.grandtotal.revenue.toFixed(2));
                $('#grandTotal_promotion').html('$' + data.grandtotal.discount.toFixed(2));
                $('#grandTotal_paid').html('$' + data.grandtotal.paid.toFixed(2));
                $('#grandTotal_commission').html('$' + data.grandtotal.commission.toFixed(2));
                if (data.grandtotal.credit > 0) {
                    $('#grandTotal_credit_toshow').show();
                    $('#grandTotal_credit').html('$' + data.grandtotal.credit);
                } else {
                    $('#grandTotal_credit_toshow').hide();
                }
                if (data.grandtotal.debit < 0) {
                    $('#grandTotal_debit_toshow').show();
                    $('#grandTotal_debit').html('$' + data.grandtotal.debit);
                } else {
                    $('#grandTotal_debit_toshow').hide();
                }
                $('#grandTotal_discount').html('$' + data.grandtotal.discount.toFixed(2));
            } else {
                //Load Grand Total div
                $('#grandTotal_fulfilled').html('$ NA');
                $('#grandTotal_revenue').html('$' + val.toFixed(2));
                $('#grandTotal_promotion').html('$' + val.toFixed(2));
                $('#grandTotal_paid').html('$' + val.toFixed(2));
                $('#grandTotal_commission').html('$' + val.toFixed(2));
                $('#grandTotal_debit_toshow').hide();
                $('#grandTotal_credit_toshow').hide();
                $('#grandTotal_discount').html('$' + val.toFixed(2));
            }
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                    title: "",
                    text: errorMessage,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

}

function PrintElem(elem) {
    var mywindow = window.open('', 'PRINT');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('</head><body>');
    mywindow.document.write("<style>" +
        "ol.breadcrumb, .form-align{display: none;}" +
        "table.first tbody tr:last-child{display: none;}" +
        ".card{margin-top:40px;}" +
        "table.second tbody tr td:first-child, table.third tbody tr td:first-child, table.fourth tbody tr td:first-child{min-width: 200px;}" +
        "table thead th{padding: 5px 0px;}" +
        "table {width: 100%;border-collapse: collapse;}" +
        "table thead {color: #8D8D8D;font-size: 14px;font-weight: 500;border: 0;border-bottom: 1px solid #D8D8D8;vertical-align: middle;padding-bottom: 15px;}" +
        "table tbody {text-align: center;}" +
        "table td, table th{border: 1px solid #ddd;}" +


        "</style>");
    // mywindow.document.write('<h1>' + document.title + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}