/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

$(function () {
    $(".datepicker-head").datepicker({
        dateFormat: 'mm/dd/yy'
    });
});

var Promotions = Array();


$(document).ready(function () {

    $(".datepicker").datepicker({
        dateFormat: 'mm/dd/yy'
    });

    startDate = moment().startOf('month').format("MM/DD/YYYY");
    endDate = moment().endOf('month').format("MM/DD/YYYY");
    $("#startDate").val(startDate);
    $("#endDate").val(endDate);

    loadPromo(startDate, endDate, true);

    
    /* check status */
    var status = $("#status").val();
    if (status == 'true') {
        $(".card-head h3").html("List of Live Promotions");
        $("#total").hide();
        $("#title").text("Days Left");
    } else {
        $(".card-head h3").html("List of Ended Promotions");
        $("#total").show();
        $("#title").text("Days");
    }
});

function loadPromo(startDate, endDate, status) {
    //Fetch live promotions
    $.ajax({
        url: base_url + 'superadmins/promotionStoreList',
        data: {
            "startDate": moment(startDate).format('YYYY-MM-DD'),
            "endDate": moment(endDate).format('YYYY-MM-DD'),
            "active": status,
            "access_token": localStorage.getItem("accessToken")
        },
        type: "GET",
        dataType: "json",
        success: function (data) {
            Promotions = data;
            Promotions = sortResults(Promotions, 'updatedAt', false); //Sort orders
            loadPromotions(Promotions, status);
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });

}


function loadPromotions(promotion, status) {
    //console.log(promotion);
    var list = '';
    if (status == true) {
        className = 'col-danger';
        diff = 'days';
    } else {
        className = 'col-primary';
        diff = 'day';
    }
    $("#livePromo").html('');
    if (promotion.length > 0) {
        //  $("#livePromo").html('');
        $.each(promotion, function (key, value) {
            if (value.discAmount > 0) {
                discountAmount = 'flat $' + value.discAmount + ' off';
            } else {
                discountAmount = 'flat ' + value.discPercentage + '% off';
            }

            list = '<tr>' +
                '<td>' + value.code + '</td>' +
                '<td>' + value.name + '</td>' +
                '<td>' + discountAmount + '</td>' +
                '<td>' +
                '<span>Start:</span>' + moment(value.startsAt).format("MMM DD") +
                '<br>' +
                '<span>End:</span>' + moment(value.endsAt).format("MMM DD") + '</td>' +
                '<td class="' + className + '">' + moment(value.endsAt).toNow(true) + '</td>' + // moment(value.endsAt).toNow(true) //moment().diff(value.endsAt, "days") + diff 
                '<td><a style="cursor: pointer;" onclick="EditPromotionBtn(\'' + value.id + '\')" data-toggle="modal" >Edit</a></td>' +
                '<td><a style="cursor: pointer;" onclick="DeletePromotionBtn(\'' + value.id + '\')" data-toggle="modal">Delete</a></td>' +
                '</tr>';
            $("#livePromo").append(list);
        });
    } else {
        list = '<tr>' +
            '<td colspan="5" style="text-align:center">No Data Found !</td>' +
            '</tr>';
        $("#livePromo").append(list);
    }

    //Load total data
    total = '<tr>' +
        '<td>' +
        '<strong>Total</strong>' +
        '</td>' +
        '<td> NA </td>' +
        '<td> NA </td>' +
        '<td>' +
        '<span>Start:</span> NA' + // moment(value.startsAt).format("MMM DD") +
        '<br>' +
        '<span>End:</span> NA' + /*moment(value.endsAt).format("MMM DD") + */ '</td>' +
        '<td>Debit:' +
        '<span class="col-danger"> NA</span>' +
        '</td>' +
        '</tr>';
    $("#totalPromo").html(total);

}


$("#status").on('change', function () {
    status = $(this).val();

    if (status == 'true') {
        $(".card-head h3").html("List of Live Promotions");
        $("#total").hide();
        $("#title").text("Days Left");
    } else {
        $(".card-head h3").html("List of Ended Promotions");
        $("#total").show();
        $("#title").text("Days");
    }
    if ($("#startDate").val() == null || $("#endDate").val() == null) {
        startDate = moment().startOf('month').format("MM/DD/YYYY");
        endDate = moment().endOf('month').format("MM/DD/YYYY");
    } else {
        startDate = $("#startDate").val();
        endDate = $("#endDate").val();
    }
    $("#startDate").val(startDate);
    $("#endDate").val(endDate);

    loadPromo(startDate, endDate, status);
});


$(".datepicker-head").on("change", function () {

    startDate = $("#startDate").val();
    endDate = $("#endDate").val();

    if (startDate > endDate) {
        swal({
                title: "",
                text: dateErrorMessage,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true,
            },
            function () {
                startDate = moment().startOf('month').format("MM/DD/YYYY");
                endDate = moment().endOf('month').format("MM/DD/YYYY");
                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                loadPromo(startDate, endDate, 'true');
            });
    } else {
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
        loadPromo(startDate, endDate, 'true');
    }
});

function EditPromotionBtn(promotionId) {
    data = $.grep(Promotions, function (i, d) {
        return i.id == promotionId;
    })

    data = data[0]; //Array to object
    //set dropdown
    if(data.discAmount > 0){
        $(".discountBy").val('A');
        amount = data.discAmount;
    }else{
        $(".discountBy").val('P');
        amount = data.discPercentage;
    }
    $("#editPromotionStartsAt").val(moment(data.startsAt).format("MM/DD/YYYY"));
    $("#editPromotionEndsAt").val(moment(data.endsAt).format("MM/DD/YYYY"));
    $("#editPromotionMinimumAmt").val(data.minimumAmt);
    $("#editPromotionDiscAmount").val(amount);
    $("#editPromotionCode").val(data.code);
    $("#editPromotionName").val(data.name);
    $("#editPromotionSave").attr('data-id', data.id);
    $("#myModal-edit-promotions").modal('show');
}

$("#editPromotionSave").click(function () {
    id = $(this).attr('data-id');

    //Added code to end date add hours so same date will expire promotion at end of day to 23:59:59
    endDate = moment($("#editPromotionEndsAt").val()).format("YYYY-MM-DDT23:59:59");
    console.log(endDate);
    endDate = moment.tz(endDate, Intl.DateTimeFormat().resolvedOptions().timeZone).tz("UTC").format("YYYY-MM-DD HH:mm:ss")
    console.log(endDate);
    code = $("#editPromotionCode").val();
    name = $("#editPromotionName").val();
    startsAt = $("#editPromotionStartsAt").val();
    endsAt = $("#editPromotionEndsAt").val();
    minAmount = $("#editPromotionMinimumAmt").val();
    disAmount = $("#editPromotionDiscAmount").val();
    if($(".discountBy").val() == 'A'){
        disAmt = disAmount;
        discPercentage = 0;
    }
    else{
        disAmt = 0;
        discPercentage = disAmount;
    }
    $.ajax({
        url: base_url + 'promotions/' + id,
        type: "PATCH",
        dataType: "json",
        data: {
            "code": code,
            "name": name,
            "startsAt": startsAt,
            "endsAt": endDate,
            "minimumAmt": minAmount,
            "discAmount": disAmt,
            "discPercentage" : discPercentage
        },
        success: function (response) {
            console.log(response);
            $("#myModal-edit-promotions").modal('hide');
            location.reload();
        }
    });
});

function DeletePromotionBtn(promotionId) {
    data = $.grep(Promotions, function (i, d) {
        return i.id == promotionId;
    })

    data = data[0]; //Array to object
    if(data.discAmount > 0){
        $(".discountBy").val('A');
        amount = data.discAmount;
    }else{
        $(".discountBy").val('P');
        amount = data.discPercentage;
    }
    $("#deletePromotionStartsAt").val(moment(data.startsAt).format("MM/DD/YYYY"));
    $("#deletePromotionEndsAt").val(moment(data.endsAt).format("MM/DD/YYYY"));
    $("#deletePromotionMinimumAmt").val(data.minimumAmt);
    $("#deletePromotionDiscAmount").val(amount);
    $("#deletePromotionCode").val(data.code);
    $("#deletePromotionName").val(data.name);
    $("#deletePromotionSave").attr('data-id', data.id);
    $("#myModal-delete-promotions").modal('show');
}

$("#deletePromotionSave").click(function () {
    id = $(this).attr('data-id');
    $.ajax({
        url: base_url + 'promotions/' + id,
        type: "PATCH",
        dataType: "json",
        data: {
            "active": false ///discussed with sanchit to keep it false for now (21-Mar-2018)
        },
        success: function (response) {
            //console.log(response);
            $("#myModal-delete-promotions").modal('hide');
            location.reload();
        }
    });
});