/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global base_url */

/* Authorization */
var userid = localStorage.getItem('superUserId');
if (userid !== null) {
    window.location = 'home';
}


function login(event) {

    event.preventDefault(); //prevent page refresh
    var email = $('#email').val();
    var password = $('#password').val();

    $.ajax({
        url: base_url + "superadmins/login",
        dataType: "json",
        type: "post",
        data: {
            "email": email,
            "password": password
        },
        error: function (error) {
            alert("email or password are incorrect");
            window.location = "login";
        },
        success: function (result) {

            if (result.userId) {
                localStorage.setItem("accessToken", result.id);
                localStorage.setItem("superUserId", result.userId);
                window.location = 'home';
                ///Delete all existing devices of user
                // $.ajax({
                //     url: base_url + 'superadmins/' + result.userId + '/devices?access_token=' + localStorage.getItem('accessToken'),
                //     type: 'delete',
                //     complete: function (params) {
                //         if (params.status == 204) {

                //             ///Added push token api call here
                //             //Add last login device detail
                //             $.ajax({
                //                 url: base_url + 'superadmins/' + result.userId + '/devices?access_token=' + localStorage.getItem('accessToken'),
                //                 type: 'POST',
                //                 data: {
                //                     "uuid": localStorage.getItem("deviceId"),
                //                 },
                //                 success: function (param) {
                //                     console.log(param);
                //                     window.location = 'home';
                //                 }
                //             });
                //         } else {
                //             alert("Request Not completed");
                //         }
                //     }
                // });
                //window.location = "home";
            }
        }
    });
}