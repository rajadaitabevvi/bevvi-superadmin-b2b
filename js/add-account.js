/* Authorization */
/* global base_url, access_token, moment, errorMessage */

var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}
var stores = Array();
var validate = true;

$(".reqd-red").hide();
/*store hours*/
var days = {
    2: "Monday",
    3: "Tuesday",
    4: "Wednesday",
    5: "Thursday",
    6: "Friday",
    7: "Saturday",
    1: "Sunday"
};

function timeConvert(time) {
    if (time === '' || time.trim() === '') {
        time = "00:00 AM";
    }
    return moment(time, 'h:m A').format('HHmm');
}

/*
$("input").on("blur", function () {
    $('input').each(function () {
        if (!$(this).val().trim()) {
            $(".btn").removeClass("btn-save");
            $(".reqd-red").show();
            $(".btn").addClass("btn-create");
            $(this).focus();
            return validate = false;
        } else {
            $(".btn").removeClass("btn-create");
            $(".reqd-red").hide();
            $(".btn").addClass("btn-save");
            return validate = true;
        }
    });
});

*/
$("#ai_email").on("blur", function () {
    email = $("#ai_email").val().trim();
    if (email !== null) {
        $.ajax({
            url: base_url + 'bizaccounts?filter={"where":{"email":"' + email + '"}}&access_token=' + access_token,
            type: "GET",
            dataType: "json",
            complete: function (response) {
                console.log("complete");
                console.log(response);
                if (response.status === 200) {
                    msg = JSON.parse(response.responseText);

                    if (msg.length > 0) {
                        swal({
                            title: "",
                            text: '"' + email + '"' + "- Email Already Exist !",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                $("#ai_email").val('');
                                $("#ai_email").focus();
                            });
                    } else {
                        validate = true;
                        $(".btn").removeClass("btn-create");
                        $(".reqd-red").hide();
                        $(".btn").addClass("btn-save");
                    }
                } else {
                    console.log("complete");
                    console.log(response);
                }
            }
        });
    } else {
        $("#ai_email").focus();
    }
});

function validate_form() {
    validate = true;

    if ($("#ai_name").val().trim() == '') {
        $("#ai_name").focus();
        validate = false;
    } else if ($("#ai_email").val().trim() == '') {
        $("#ai_email").focus();
        validate = false;
    } else if ($("#ai_phone").val().trim() == '') {
        $("#ai_phone").focus();
        validate = false;
    } else if ($("#si_name").val().trim() == '') {
        $("#si_name").focus();
        validate = false;
    } else if ($("#si_address").val().trim() == '') {
        $("#si_address").focus();
        validate = false;
    }

    if (validate) {
        return true;
    } else {
        $(".btn").removeClass("btn-save");
        $(".reqd-red").show();
        $(".btn").addClass("btn-create");
        return false;
    }
}

$(".btn").click(function () {
    var validate = validate_form();
    //validate form

    // $('form input').each(function () {
    //     console.log($(this).attr('id'), $(this).val().trim());

    //     if ($(this).val().trim() === '') {
    //         $(".btn").removeClass("btn-save");
    //         $(".reqd-red").show();
    //         $(".btn").addClass("btn-create");
    //         $(this).focus();
    //         return validate = false;
    //     } else {
    //         return validate = true;
    //     }
    // });
    if (validate === true) { //Success Validation
        //Admin info
        ai_name = $("#ai_name").val();
        ai_email = $("#ai_email").val();
        ai_phone = $("#ai_phone").val();
        //Store info
        si_name = $("#si_name").val();
        si_address = $("#si_address").val();
        si_phnumber = $("#si_phnumber").val();
        si_email = $("#si_email").val();
        si_additional_email = $("#si_additional_email").val();
        si_website = $("#si_website").val();

        var mon = [timeConvert($('#mon .start').val()), timeConvert($('#mon .end').val())];
        var tue = [timeConvert($('#tue .start').val()), timeConvert($('#tue .end').val())];
        var wed = [timeConvert($('#wed .start').val()), timeConvert($('#wed .end').val())];
        var thu = [timeConvert($('#thu .start').val()), timeConvert($('#thu .end').val())];
        var fri = [timeConvert($('#fri .start').val()), timeConvert($('#fri .end').val())];
        var sat = [timeConvert($('#sat .start').val()), timeConvert($('#sat .end').val())];
        var sun = [timeConvert($('#sun .start').val()), timeConvert($('#sun .end').val())];

        //In mobile week sunday to saturday
        var schedule = {
            "timeframes": [{
                "days": [
                    2
                ],
                "open": [{
                    "start": mon[0],
                    "end": mon[1]
                }]
            },
            {
                "days": [
                    3
                ],
                "open": [{
                    "start": tue[0],
                    "end": tue[1]
                }]
            },
            {
                "days": [
                    4
                ],
                "open": [{
                    "start": wed[0],
                    "end": wed[1]
                }]
            },
            {
                "days": [
                    5
                ],
                "open": [{
                    "start": thu[0],
                    "end": thu[1]
                }]
            },
            {
                "days": [
                    6
                ],
                "open": [{
                    "start": fri[0],
                    "end": fri[1]
                }]
            },
            { //Saturday
                "days": [
                    7
                ],
                "open": [{
                    "start": sat[0],
                    "end": sat[1]
                }]
            },
            { //Sunday
                "days": [
                    1
                ],
                "open": [{
                    "start": sun[0],
                    "end": sun[1]
                }]
            }
            ]
        };

        //Add Bizaccount
        $.ajax({
            url: base_url + 'bizaccounts?access_token=' + access_token,
            data: {
                firstName: ai_name,
                email: ai_email,
                phoneNum: ai_phone
                // accountNum: si_address
                //establishmentId :[localStorage.getItem('establishmentId')]
            },
            type: "POST",
            dataType: "json",
            success: function (data) {
                console.log("bizaccount added !");
                console.log(data);
                bizAccountId = data.id;
                //Add Establishment
                $.ajax({
                    url: base_url + 'establishments?access_token=' + access_token,
                    data: {
                        name: si_name,
                        phoneNum: si_phnumber,
                        email: si_email,
                        ccList: si_additional_email,
                        url: si_website,
                        operatesAt: schedule,
                        bizAccountId: bizAccountId, //bizAccountId
                        address: si_address,
                        accountId: "123465",
                        locationId: "5a94e09f383b4c278dd0548c",
                        bevviActive: true //Discussed with sanchit to set true by default
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (e_data) {
                        console.log("establishment added !");
                        console.log(e_data);
                        establishmentId = e_data.id;
                        console.log(establishmentId);

                        //Update bizaccount detail
                        $.ajax({
                            url: base_url + 'bizaccounts/' + bizAccountId + '?access_token=' + access_token,
                            data: {
                                establishmentIds: [establishmentId]
                            },
                            type: "PATCH",
                            dataType: "json",
                            success: function (data) {
                                console.log(data);

                                swal({
                                    title: "",
                                    text: "Account Added Successfully.",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                    function () {
                                        //alert("Account Added Successfully.");
                                        window.location = localStorage.getItem('backToPage');
                                    });

                            },
                            error: function (response) {

                                console.log(response);
                                if (response.status === 401) {
                                    swal({
                                        title: "",
                                        text: errorMessage,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: false
                                    },
                                        function () {
                                            localStorage.removeItem("superUserId");
                                            window.location = 'login';
                                        });
                                } else if (response.status === 422) {
                                    swal({
                                        title: "",
                                        text: response.responseJSON.error.message,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            $("#ai_name").focus();
                                        });
                                }
                            }
                        });
                    },
                    error: function (response) {

                        console.log(response);
                        if (response.status === 401) {
                            swal({
                                title: "",
                                text: errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: false
                            },
                                function () {
                                    localStorage.removeItem("superUserId");
                                    window.location = 'login';
                                });
                        } else if (response.status === 422) {
                            swal({
                                title: "",
                                text: response.responseJSON.error.message,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    $("#ai_name").focus();
                                });
                        }
                    }
                });
            },
            error: function (response) {

                console.log(response);
                if (response.status === 401) {
                    swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                        function () {
                            localStorage.removeItem("superUserId");
                            window.location = 'login';
                        });
                } else if (response.status === 422) {
                    swal({
                        title: "",
                        text: response.responseJSON.error.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            $("#ai_name").focus();
                        });
                }
            }
        });
    }
});


$("#si_name").on("blur", function () {
    storeName = $(this).val().trim();
    if (storeName === '') {
        $("#si_address").val('');
        $("#si_phnumber").val('');
        $("#si_email").val('');
        $("#si_website").val('');
    } else {
        $.ajax({
            url: base_url + 'establishments?filter={"where":{"name":{"like":"' + storeName + '"}}}', //,"options":"i" - it will find substring
            success: function (data) {
                if (data.length > 0) {
                    console.log(data[0]);
                    loadStoreDetail(data[0]);
                } else {
                    $("#si_address").val('');
                    $("#si_phnumber").val('');
                    $("#si_email").val('');
                    $("#si_website").val('');
                }
            }
        });
    }
});

function loadStoreDetail(store) {
    $("#si_address").val(store.address);
    $("#si_phnumber").val(store.phoneNum);
    $("#si_email").val(store.email);
    $("#si_website").val(store.url);

    ////////// for store hours
    $.each(store.operatesAt.timeframes, function (i, time) {
        var start_time = time.open[0].start;
        var end_time = time.open[0].end;

        $.each(time.days, function (k, day) {
            switch (parseInt(day)) {
                case 2:
                    $("#mon .start").val(moment(start_time, 'h:mm a').format('h:mm A')); // 10:00 AM
                    $("#mon .end").val(moment(end_time, 'h:mm a').format('h:mm A')); // 8:00 PM
                    break;
                case 3:
                    $("#tue .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#tue .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
                case 4:
                    $("#wed .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#wed .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
                case 5:
                    $("#thu .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#thu .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
                case 6:
                    $("#fri .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#fri .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
                case 7:
                    $("#sat .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#sat .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
                case 1:
                    $("#sun .start").val(moment(start_time, 'h:mm a').format('h:mm A'));
                    $("#sun .end").val(moment(end_time, 'h:mm a').format('h:mm A'));
                    break;
            }
        });
    });
}