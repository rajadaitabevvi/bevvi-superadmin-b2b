/* Authorization */
var userid = localStorage.getItem('superUserId');
if (!userid) {
    window.location = 'login';
}

$(document).ready(function () {

    $.ajax({
        //url: base_url + 'orders/searchByOrderNum?orderNum=' + params,
        url: base_url + 'orders?filter={"where": {"orderNumber": "' + params + '"}, "include":["account","establishment",{"orderdetails":["product"]}]}',
        success: function (data) {
            console.log(data);
            data = data[0];

            //dynamic breadcrumb
            breadcrumb = '<li><a href="manage-orders">Manage Orders </a></li>' +
                '<li><a href="view-orders?id=' + data.establishmentId + '">View Orders </a></li>' +
                '<li><a href="live-orders?id=' + params + '">Order Detail </a></li>' +
                '<li class="active">View Receipt</li>';
            $(".breadcrumb").html(breadcrumb);

            $('.orderNumber').html(data.orderNumber);
            $('#personName').html(data.qty + ' Items for ' + data.account.firstName);

            $('#listingData').html('');
            subtotal = 0;
            $.each(data.orderdetails, function (key, value) {
                var list = '<tr>' +
                    '<td>' + value.product.name +
                    '</td>' +
                    '<td>' + value.quantity + '</td>' +
                    '<td>$' + value.price.toFixed(2) + '</td>' +
                    '<td>' + moment(data.deliveryTime).format('MMM D. dddd / ha') + '</td>' +
                    '</tr>';
                subtotal = subtotal + (value.quantity * parseFloat(value.price));
                $('#listingData').append(list);
            });
            if(data.tax != null){
                tax = data.tax.toFixed(2);
            }
            else{
                tax = '0.0';
            }
            $('#subTotal').html('$' + subtotal.toFixed(2));
            $('#tax').html('$' + tax);
            $('#paid').html('$' + data.totalAmount.toFixed(2));
        },
        error: function (response) {
            console.log(response);
            if (response.status == 401) {
                swal({
                        title: "",
                        text: errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function () {
                        localStorage.removeItem("superUserId");
                        window.location = 'login';
                    });
            }
        }
    });
});

//Open print Dialog Box
$(".btn-print").click(function () {

    $(".mo-head").hide();
    $("aside").hide(); //Sidebar
    $("header").hide(); //header
    $(".btn-print").hide();
    $(".card").show(); //Show area of print

    window.print();
    $(".mo-head").show();
    $("aside").show(); //Sidebar
    $("header").show(); //header
    $(".btn-print").show();
});